<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WebmasterTag;
use App\SiteSetting;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    //
    public function index(){
        
        $webmaster = WebmasterTag::all();
        foreach($webmaster as $data){
            config(['seotools.meta.webmaster_tags.'.$data->column_name.'' => $data->value ]);
        }
        $seo = SiteSetting::all();

        foreach($seo as $data){
            config(['meta.'.$data->name.'' => $data->value ]);
        }


        if (Auth::check()) {
            return redirect('sukses');
        } else {
            return view('index.index_temp');
        }

    }
    public function home(){
        /*
        $webmaster = WebmasterTag::all();

        foreach($webmaster as $data){
            config(['seotools.meta.webmaster_tags.'.$data->column_name.'' => $data->value ]);
        }

        $seo = SiteSetting::all();
        foreach($seo as $data){
            config(['meta.'.$data->name.'' => $data->value ]);
        }
        */

        return view('index.under');
    }

    public function faq(){
        return view('index.faq');
    }

    public function donasi(){
        return view('index.donasi');
    }

}
