<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    //
    public function show($nomor_iden){
        $data = User::where('nomor_iden', $nomor_iden)->firstorfail();
        //return response()->json($data);
        return view('dashboard.profile', compact('data'));
    }
    public function update(Request $request){
        $request->validate([
            'name'      => 'required|string',
            'email'     => 'required|email',
        ]);

        $user = Auth::user();

        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->update();

        return back()->with("status", "Profile berhasil diupdate");
    }

    public function passwordform(Request $request)
    {
        //
        $user = Auth::user();
        return view('dashboard.password');
    }

    public function changePassword(Request $request)
    {
        //
        //return response()->json($request->all());
        //oldpassword: "asdas", newpassword: "sdasd", newpassword_confirmation: "adasd"
        
        if (!(Hash::check($request->get('oldpassword'), Auth::user()->password))) {
            // The passwords not matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('oldpassword'), $request->get('newpassword')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'oldpassword' => 'required',
            'newpassword' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->get('newpassword'));
        $user->save();

        //return back()->with("status", "Password berhasil diganti !");
        return redirect($user->nomor_iden.'/profile')->with('status', 'Password Berhasil diupdate');
        
    }

}
