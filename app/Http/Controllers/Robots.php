<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Robots extends Controller
{
    //
    public function index(){
        /*
        $file = public_path('robots.txt');
        $read = file_get_contents($file);
        return view('robot', compact('read'));
        */
        try {
            $file = public_path('robots.txt');
            $read = file_get_contents($file);
       
            if ($read === false) {
                // Handle the error
            } else {
                return view('robot', compact('read'));
            }

        } catch (Exception $e) {
            // Handle exception
        }
    }

    public function save(Request $request){
        file_put_contents(public_path('robots.txt'), $request->robot_txt);
        return back()->with('status', 'file robot sukses diupdate');
    }

}