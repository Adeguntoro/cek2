<?php

namespace App\Http\Controllers;

use App\SiteSetting;
use Illuminate\Http\Request;
use Spatie\Sitemap\SitemapGenerator;
//
use Carbon\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class SiteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function sitemap()
    {
        //
        $path = public_path('sitemap.xml');
        $url = 'https://ceklapor.com';

        SitemapGenerator::create($url)
            ->getSitemap()
            ->add(Url::create('/')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                ->setPriority(1.0))
            ->add(Url::create('/login')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                ->setPriority(0.5))
            ->add(Url::create('/register')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                ->setPriority(0.1))
            ->add(Url::create('/home')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                ->setPriority(0.1))
            ->add(Url::create('/review')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
                ->setPriority(0.1))
            ->add(Url::create('/about/us/')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
                ->setPriority(0.1))
            ->add(Url::create('/about/us/')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
                ->setPriority(0.1))
        ->writeToFile($path);
    }

    public function index()
    {
        //
        //$datas = SiteSetting::all();
        $datas = SiteSetting::where('name_key','<>','image_key')->get();
        return view('dashboard.setting.config', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $settings = $request->settings;
        foreach ($settings as $key => $value ) {
            SiteSetting::where(['name_key' => $key])->update(['value'=> $value['value']]);
        }
        return back()->with('status', 'sukses!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function show(SiteSetting $siteSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteSetting $siteSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteSetting $siteSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteSetting $siteSetting)
    {
        //
    }

    public function formAdd()
    {
        //r
        return view('dashboard.setting.site');
    }

    public function formStore(Request $request)
    {
        //
        SiteSetting::create([
            'name' => $request->name,
            'value' => $request->value,
            'name_key' => $request->name_key,
            'info' => $request->info,
            'name_info' => $request->name_info,
            'column_name' => $request->column_name,
        ]);

        return back()->with('status', 'disimpan');
    }

    public function webImage(Request $request)
    {
        //$user = Auth::user();
        return view('dashboard.setting.web_image');
    }

    public function updateAvatar(Request $request){
        $image      = $request->image;

        $user       = Auth::user();

        $unique = bin2hex(random_bytes(strlen($user->email)));
        
        preg_match('/data:image\/(?<mime>.*?)\;/', $image, $groups);
        $mimeType = $groups['mime'];

        $name = $user->nomor_iden.'_'.$unique.'.' . $mimeType;
        $path = '/avatar/'.$name;
        Image::make($image)->encode($mimeType, 100)->save(public_path($path));

        $user->avatar   = $path;
        $user->save();

        return response()->json(['status' => 'success','msg' => 'format file adalah = '.$mimeType]);
        
        
    }


}
