<?php

namespace App\Http\Controllers;

use App\WebmasterTag;
use Illuminate\Http\Request;

class WebmasterTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datas = WebmasterTag::all();
        /*
        foreach($datas as $data){
        
        }
        */
        return view('webmaster', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $settings = $request->settings;
        foreach ($settings as $key => $value ) {
            WebmasterTag::where(['name_key' => $key])->update(['value'=> $value['value']]);
        }
        return back()->with('status', 'sukses!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WebmasterTag  $webmasterTag
     * @return \Illuminate\Http\Response
     */
    public function show(WebmasterTag $webmasterTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WebmasterTag  $webmasterTag
     * @return \Illuminate\Http\Response
     */
    public function edit(WebmasterTag $webmasterTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebmasterTag  $webmasterTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebmasterTag $webmasterTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebmasterTag  $webmasterTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebmasterTag $webmasterTag)
    {
        //
    }
}
