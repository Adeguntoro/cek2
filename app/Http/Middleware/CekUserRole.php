<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CekUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        //Blogger User

        if (Auth::user()->hasRole('Master')){
            return redirect('/dashboard/ocisly');
        }
        
        elseif (Auth::user()->hasRole('Staff')){
            return redirect('/dashboard/staff');

        }
        elseif (Auth::user()->hasRole('Admin')){
            return redirect('/dashboard/admin');
        }
        elseif (Auth::user()->hasRole('Blogger')){
            return redirect('/dashboard/blogger');
        }
        elseif (Auth::user()->hasRole('User')){
            return redirect('/home');
        }

        return $response;
    }
}
