<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UserAktif
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        
        //0 aktivasi, 1 aktif, 2 banned

        if(Auth::user()->status == 0){
            return redirect('aktivasi');
        }
        elseif (Auth::user()->status== 1){
            return redirect('sukses');
        }
        elseif (Auth::user()->status == 2){
            return redirect('nonaktif');
        }
        return $response;
    }
}
