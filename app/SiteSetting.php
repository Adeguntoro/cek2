<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    //
    protected $fillable = [
        'name', 'value', 'name_key', 'info', 'name_info', 'column_name', 'type_input', 'status'
    ];

    public $timestamps = false;
}
