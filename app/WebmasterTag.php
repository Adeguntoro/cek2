<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebmasterTag extends Model
{
    //
    protected $fillable = [
        'name', 'value', 'name_key', 'info', 'name_info'
    ];

    public $timestamps = false;
}
