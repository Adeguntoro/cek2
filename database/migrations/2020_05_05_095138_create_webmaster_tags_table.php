<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebmasterTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webmaster_tags', function (Blueprint $table) {
            //'name', 'value', 'name_key', 'info', 'name_info'
            $table->id();
            $table->string('name');
            $table->string('value');
            $table->string('name_key');
            $table->string('info');
            $table->string('name_info');
            $table->string('column_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webmaster_tags');
    }
}
