@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">Site Settings</div>

                <div class="card-body">
                    <a href="/upload_web_image">Edit your website image here</a>
                    <hr>
                    <form method="POST" action="{{ route('site.store') }}">
                        @csrf
                        @foreach($datas as $data)
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">{{ $data->column_name }}</label>
                            @if($data->type_input == 'input')
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" name="settings[{{ $data->name_key }}][value]" value="{{ $data->value }}">
                            </div>
                            @elseif($data->type_input == 'textarea')
                            <div class="col-sm-10">
                                <textarea class="form-control" id="inputEmail3" name="settings[{{ $data->name_key }}][value]" rows="4">{{ $data->value }}</textarea>
                            </div>
                            @elseif($data->type_input == 'option')
                            <div class="col-sm-10">
                                <select class="form-control" id="exampleFormControlSelect1" name="settings[{{ $data->name_key }}][value]">
                                    <option selected value="{{ $data->value }}">{{ $data->value }}</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            @endif
                        </div>
                        @endforeach
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
