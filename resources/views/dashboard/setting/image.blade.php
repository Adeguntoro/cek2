@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ url('/vendor/Croppie-2.6.3/croppie.css') }}">
<style>
    .user-avatar {

        margin: 2em auto;

    }
</style>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Avatar</div>
                <div class="card-body">
                    <div class="text-center">
                        <input type="file" id="image" accept="image/jpeg">
                        <div id="upload-demo" class="pt-4 pb-4"></div>
                        <div class="pt-6 pb-3">
                            <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                            <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                        </div>
                        <button class="btn btn-primary btn-block upload-image" style="margin-top:2%">Upload Image</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
