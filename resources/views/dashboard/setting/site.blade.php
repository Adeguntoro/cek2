@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">Site Settings</div>

                <div class="card-body">
                    <form method="POST" action="/setting/add/site">
                        @csrf
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" name="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Value</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" name="value">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Name Key</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" name="name_key">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Info</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="4" name="info"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Name Info</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" name="name_info">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Column Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" name="column_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Type Input</label>
                            <div class="col-sm-10">
                                {{--<input type="text" class="form-control" id="inputEmail3" name="column_name"> --}}
                                <select class="form-control" name="type_input">
                                    <option>Input</option>
                                    <option>TextArea</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
