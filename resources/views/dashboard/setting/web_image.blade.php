@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ url('/vendor/Croppie-2.6.3/croppie.css') }}">
<style>
    .user-avatar {

        margin: 2em auto;

    }
</style>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Avatar</div>
                <div class="card-body">
                    <div class="text-center">
                        <input type="file" id="image" accept="image/jpeg">
                        <div id="upload-demo" class="pt-4 pb-4"></div>
                        <div class="pt-6 pb-3">
                            <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                            <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                        </div>
                        <button class="btn btn-primary btn-block upload-image" style="margin-top:2%">Upload Image</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ url('/vendor/exif/exif.js')}}"></script>
<script src="{{ url('/vendor/Croppie-2.6.3/croppie.min.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(document).ready(function () {
        'use strict';
        
        var el = $('#upload-demo');
        var resize = el.croppie({

            enableExif: true,
            enableOrientation: true,
            viewport: {
                width: 230,
                height: 230,
                type: 'square'
            },
            boundary: {
                width: 240,
                height: 240
            }
        });

        $('#image').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                resize.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            };
            reader.readAsDataURL(this.files[0]);
        });

        $('.vanilla-rotate').on('click', function (ev) {
            ev.preventDefault();
            el.croppie('rotate', parseInt($(this).data('deg')));
        });

        $('.upload-image').on('click', function (ev) {
            resize.croppie('result', {
                type: 'base64',
                size: 'original',
                quality: '1'
            }).then(function (img) {
                $.ajax({
                    url: "{{ url('/')}}/crop_image",
                    type: "POST",
                    data: {"image":img},
                    success: function (data) {
                        console.log(data.msg);
                        //window.alert(data.msg);
                        window.location.replace("{{ url('/profile') }}")
                    }
                });
            });
        });
    });

</script>
@endsection