<!doctype html>
<html lang="en">
    <head>
        <!-- <title>Dogger &mdash; Website Template by Colorlib</title> -->
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
        @yield('meta')
        <link href="{{ url('/fonts/font.css') }}" rel="stylesheet">
        <link href="{{ url('/css/all.css')}}" rel="stylesheet">
        <style>

            .h2, h1, h2, h3, h4, h5 {
                font-family: 'Catamaran-ExtraBold' !important;
            }
            footer h2{
                font-family: 'Catamaran-Light' !important
            }

            .sticky-wrapper.is-sticky .site-navbar .site-menu > li > a:hover, .sticky-wrapper.is-sticky .site-navbar .site-menu > li > a.active {
                color: #dc3545 !important;
            }
            
            .sticky-wrapper .site-navbar .site-menu > li > a:hover,
            .sticky-wrapper .site-navbar .site-menu > li > a.active {
                color: #dc3545 !important;
            }
            .batas_atas {
                padding-top: 40px;
            }
        </style>
    </head>
    
    <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" id="home-section">
        <div id="overlayer"></div>
        
        <div class="loader">
            <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Sedang Membuka.....</span>
            </div>
        </div>
        
        <div class="site-wrap">
            <div class="site-mobile-menu site-navbar-target">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div>
            
            @include('index.navbar_link')

            @yield('content')
    
        </div> <!-- .site-wrap -->

        <script src="{{ url('/js/all.js') }}"></script>
        @yield('js')
        
    </body>
</html>