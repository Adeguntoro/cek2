@extends('index.app')

@section('content')
<div class="site-mobile-menu site-navbar-target">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div>

@include('index.navbar')

<section class="site-section" id="services-section">
    <div class="container">
        <div class="row justify-content-center batas_atas" data-aos="fade-up">
            <div class="col-lg-6 text-center heading-section mb-5">
                <h2 class="text-black mb-2">Cek Kendaraan<span class="text-danger">.</span></h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-md-6 text-center mb-3" data-aos="fade-up" data-aos-delay="">
                <div class="block_service">
                    <img src="{{ url('/tema/images/bike.svg') }}" alt="Image mb-5">
                    <h3>Motor</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <a href="/cek/kendaraan/motor" class="btn btn-primary stretched-link">Cek Sekarang...</a>
                </div>
            </div>
            <div class="col-md-6 text-center mb-3" data-aos="fade-up"  data-aos-delay="100">
                <div class="block_service">
                    <img src="{{ url('/tema/images/car.svg') }}" alt="Image mb-5">
                    <h3>Mobil</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <a href="/cek/kendaraan/mobil/form" class="btn btn-primary stretched-link">Cek Sekarang...</a>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection
