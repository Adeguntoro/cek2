<style>
    /*
    body{
        font-family: 'Catamaran-Light' !important; 
    }
    */
    .h2, h1, h2, h3, h4, h5 {
        font-family: 'Catamaran-ExtraBold' !important;
    }
    footer h2{
        font-family: 'Catamaran-Light' !important
    }
    /*
    a:hover{
        color: #dc3545 !important;
    }
    */
    .sticky-wrapper.is-sticky .site-navbar .site-menu > li > a:hover, .sticky-wrapper.is-sticky .site-navbar .site-menu > li > a.active {
        color: #dc3545 !important;
    }

    .sticky-wrapper .site-navbar .site-menu > li > a:hover,
    .sticky-wrapper .site-navbar .site-menu > li > a.active {
        color: #dc3545 !important;
    }

    i.fas,
    i.fab,
    i.far {
        margin-right: 10px;
        font-size: 1.2em;
    }
    .putih{
        color: white;
    }
</style>