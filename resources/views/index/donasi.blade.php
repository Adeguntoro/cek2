<!doctype html>
<html lang="en">
    <head>
        <title>Cek dan Lapor Barang Anda</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
        {!! SEO::generate() !!}
        @include('meta::manager')
        
        <link href="{{ url('/fonts/font.css') }}" rel="stylesheet">
        <link href="{{ url('/css/all.css')}}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ url('/vendor/fontawesome-free-5.12.1-web/css/all.min.css') }}" rel="stylesheet">
        
        
    </head>
    
    <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" id="home-section">
        <div id="overlayer"></div>
        
        <div class="loader">
            <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Sedang Membuka.....</span>
            </div>
        </div>
        
        <div class="site-wrap">
            
            <div class="site-mobile-menu site-navbar-target">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div>
            
            @include('index.navbar')

         
<section class="site-section" id="services-section">
    <div class="container pt-4">
        <div class="row justify-content-center" data-aos="fade-up">
            <div class="col-lg-8 text-center heading-section mb-5">
                <h2 class="text-black mb-2">Bantu kami dalam pengembangan layanan<span class="text-danger">.</span></h2>
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="">
                <div class="block_service text-center">
                    <p>Donasi akan digunakan untuk pengembangan layanan, pembelian hosting, dan lain sebagainya.</p>
                    <a href="https://kitabisa.com/" class="btn btn-primary stretched-link">Donasi sekarang</a>
                </div>
            </div>
        </div>
        
    </div>
</section>
            
            
        </div>
        <script src="{{ url('/js/all.js') }}"></script>
    </body>
</html>