<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-5">
                        <h2 class="footer-heading mb-4">About Us</h2>
                        <p>Cek dan Lapor adalah sebuah layanan yang berfungsi untuk mengecek dan membuat pengumuman atau laporan tentang kehilangan barang. Kami mencoba untuk memastikan bahwa barang bekas yang akan anda beli tidaklah bermasalah.</p>
                    </div>
                    <div class="col-md-3 ml-auto">
                        <h2 class="footer-heading mb-4">Quick Links</h2>
                        <ul class="list-unstyled">
                            <li><a href="#about-section" class="smoothscroll">About Us</a></li>
                            <li><a href="#trainers-section" class="smoothscroll">Trainers</a></li>
                            <li><a href="#services-section" class="smoothscroll">Services</a></li>
                            <li><a href="#testimonials-section" class="smoothscroll">Testimonials</a></li>
                            <li><a href="#contact-section" class="smoothscroll">Contact Us</a></li>
                            <li><a href="#contact-section" class="smoothscroll">Lupa Password</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h2 class="footer-heading mb-4">Follow Us</h2>
                        <ul class="list-unstyled">
                            <li><i class="fab fa-facebook"></i><a href="https://facebook.com/">Facebook</a></li>
                            <li><i class="fab fa-twitter"></i><a href="https://twitter.com/">Twitter</a></li>
                            <li><i class="fab fa-telegram"></i><a href="https://t.me/">Telegram</a></li>
                            <li><i class="fab fa-whatsapp"></i><a href="https://api.whatsapp.com/">Whatsapp</a></li>
                            <li><i class="fab fa-youtube"></i><a href="https://api.whatsapp.com/">YouTube</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        {{--
        <div class="row text-center">
            <div class="col-md-12">
                <div class="border-top pt-5">
                    <p class="copyright">
                        <small>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a> and use by Cek<span class="text-danger">&</span>Lapor
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </small>
                    </p>
                </div>
            </div>
        </div>
        --}}
    </div>
</footer>