<section class="site-section" id="contact-section">
    <div class="container">
        <div class="row justify-content-center" data-aos="fade-up">
            <div class="col-lg-6 text-center heading-section mb-5">
                <h2 class="text-black mb-2">Ingin Memulai <span class="text-danger">?</span></h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 bg-primary" data-aos="fade-up" data-aos-delay="">
                <form action="daftar" class="p-5 contact-form" method="post">
                    @csrf
                    <h2 class="h4 mb-5 heading">Pendaftaran</h2> 
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="fname">Nama Depan</label>
                            <input type="text" id="fname" class="form-control" name="fname">
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="fname">Nama Belakang</label>
                            <input type="text" id="fname" class="form-control" name="fname">
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="email">Email</label> 
                            <input type="email" id="email" class="form-control" name="email">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="submit" value="Daftar" class="btn btn-dark btn-md text-white">
                        </div>
                    </div>
                
                </form>
                
            </div>
            
            <div class="col-lg-6 bg-secondary" data-aos="fade-up" data-aos-delay="">
                <form action="{{ route('add_data.index') }}" class="p-5 contact-form" method="post">
                    @csrf
                    <h2 class="h4 mb-5 heading text-white">Masuk</h2> 

                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="fname" class="text-white">Email</label>
                            <input type="email" id="fname" class="form-control" name="email">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="email" class="text-white">Password</label> 
                            <input type="password" id="email" class="form-control" name="password">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="submit" value="Masuk" class="btn btn-primary btn-md text-white">
                            <!--<button class="btn btn-primary btn-md" type="submit">Masuk</button> -->
                            
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>