<section id="contact-section">
    <div class="container">
        
        <div class="row justify-content-center" data-aos="fade-up">
            <div class="col-lg-6 text-center heading-section mb-5">
                <h2 class="text-black mb-2">Ingin Memulai <span class="text-danger">?</span> </h2>
                <p>Jika anda ingin membuat laporan, silahkan pilih salah satu opsi dibawah ini.</p>
            </div>
        </div>
        
        <div class="row no-gutters">
            <div class="col-lg-6 " data-aos="fade-right">
                <div class="p-5 text-center">
                    <h2 class="h4 mb-5 heading text-black">Pendaftaran</h2>
                    <p>Jika anda belum punya akun dan ingin membuat laporan.</p>
                    <a class="btn btn-primary" href="register">Daftar</a>
                </div>
            </div>
            <div class="col-lg-6 " data-aos="fade-left">
                <div class="p-5 text-center">
                    
                    <h2 class="h4 mb-5 heading text-black">Login</h2>
                    <p>Jika anda sudah punya akun dan ingin membuat laporan.</p>
                    <a class="btn btn-primary" href="login">Login</a>
                </div>
            </div>
                    
        </div>
    </div>
</section>