<!doctype html>
<html lang="en">
    <head>
        <title>Cek dan Lapor Barang Anda</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
        {!! SEO::generate() !!}
        @include('meta::manager')
        
        <link href="{{ url('/fonts/font.css') }}" rel="stylesheet">
        <link href="{{ url('/css/all.css')}}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ url('/vendor/fontawesome-free-5.12.1-web/css/all.min.css') }}" rel="stylesheet">
        
        
    </head>
    
    <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" id="home-section">
        <div id="overlayer"></div>
        
        <div class="loader">
            <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Sedang Membuka.....</span>
            </div>
        </div>
        
        <div class="site-wrap">
            
            <div class="site-mobile-menu site-navbar-target">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div>
            
            @include('index.navbar')

         
            <section class="site-blocks-cover bg-light">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 align-self-center text-center text-md-left">
                            <h1>Anda Kehilangan Barang <span class="text-danger">?</span></h1>
                            <h1><span class="d-md-block">Cek <span class="text-danger">&</span>  Lapor Disini <span class="text-danger">!</span></span></h1>
                            <p class="mb-4">Kami mencoba agar transaksi pembelian barang bekas menjadi lebih aman.</p>
                            <a class="btn btn-danger text-white" href="#contact-section">Klik disini untuk memulai.</a>
                        </div>
                    </div>
                </div>
            </section>
            
            
            @include('index.layanan')
            
            @include('index.form2')
            
            @include('index.footer')
            
            
        </div>
        <script src="{{ url('/js/all.js') }}"></script>
    </body>
</html>