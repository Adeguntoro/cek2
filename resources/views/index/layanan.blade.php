<section class="site-section" id="services-section">
    <div class="container">
        <div class="row justify-content-center" data-aos="fade-up">
            <div class="col-lg-6 text-center heading-section mb-5">
                <h2 class="text-black mb-2">Layanan Kami<span class="text-danger">.</span></h2>
                {{--<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p> --}}
                <p>
                </p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="">
                <div class="block_service text-center">
                    <img src="{{ url('/tema/images/car.svg') }}" alt="Image mb-5">
                    <h3>Cek Status Kendaraan</h3>
                    <p>Seperti sepeda motor, mobil pribadi, kendaraan angkut barang, dan sebagainya</p>
                    <a href="cek/kendaraan" class="btn btn-primary stretched-link">Cek Sekarang...</a>
                </div>
            </div>
            <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up"  data-aos-delay="100">
                <div class="block_service text-center">
                    <img src="{{ url('/icon/001-responsive-design.png') }}" alt="Image mb-5">
                    <h3>Cek Status Alat Elektronik</h3>
                    <p>Seperti telepon pintar, tablet, laptop, atau TV pintar</p>
                    <a href="cek/alat" class="btn btn-primary stretched-link">Cek Sekarang...</a>
                </div>
            </div>
            <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up"  data-aos-delay="200">
                <div class="block_service text-center">
                    <img src="{{ url('/icon/007-file-1.svg') }}" alt="Image mb-5">
                    <h3>Cek Status Surat Berharga</h3>
                    <p>Seperti BPKB, sertifikat tanah, dan lain sebagainya</p>
                    <a href="cek/surat" class="btn btn-primary stretched-link">Cek Sekarang...</a>
                </div>
            </div>

            <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up"  data-aos-delay="">
                <div class="block_service text-center">
                    <img src="{{ url('/tema/images/car.svg') }}" alt="Image mb-5">
                    <h3>Lapor Kehilangan Kendaraan</h3>
                    <p>Seperti sepeda motor, mobil pribadi, kendaraan angkut barang, dan sebagainya</p>
                    <a href="/lapor/kendaraan/" class="btn btn-primary stretched-link">Lapor Sekarang...</a>
                </div>
            </div>
            <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up"  data-aos-delay="100">
                <div class="block_service text-center">
                    <img src="{{ url('/icon/001-responsive-design.svg') }}" alt="Image mb-5">
                    <h3>Lapor Kehilangan Alat Elektronik</h3>
                    <p>Seperti telepon pintar, tablet, laptop, atau TV pintar</p>
                    <a href="#" class="btn btn-primary stretched-link">Lapor Sekarang...</a>
                </div>
            </div>
            <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up"  data-aos-delay="200">
                <div class="block_service text-center">
                    <img src="{{ url('/icon/007-file-1.svg') }}" alt="Image mb-5">
                    <h3>Lapor Kehilangan Surat Berharga</h3>
                    <p>Seperti BPKB, sertifikat tanah, dan lain sebagainya</p>
                    <a href="#" class="btn btn-primary stretched-link">Lapor Sekarang...</a>
                </div>
            </div>
        </div>
        
    </div>
</section>