<header class="site-navbar js-sticky-header site-navbar-target" role="banner" >
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6 col-xl-2">
                <h1 class="mb-0 site-logo"><a href="/" class="h2 mb-0">Cek<span class="text-danger">&</span>Lapor<span class="text-danger">.</span> </a></h1>
            </div>
            <div class="col-12 col-md-10 d-none d-xl-block">
                <nav class="site-navigation position-relative text-right" role="navigation">
                    <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                        <li><a href="{{ url('/') }}/#home-section" class="nav-link">Beranda</a></li>
                        <li><a href="{{ url('/') }}/#services-section" class="nav-link">Layanan Kami</a></li>

                        @guest
                        <li><a href="{{ url('/') }}/#contact-section" class="nav-link">Memulai ?</a></li>
                        @endguest
                        
                        <li><a href="faq" class="nav-link">FAQ</a></li>
                        @auth
                        <li class="has-children">
                            <a href="#about-section" class="nav-link">{{ Auth::user()->name }}</a>
                            <ul class="dropdown">
                                <li><a href="{{ route('profile.index') }}" class="nav-link">Profile</a></li>
                                <li>
                                    <a class="nav-link" href="{{ route('logout') }}" 
                                       onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </ul>
                        </li>
                        @endauth
                    </ul>
                </nav>
            </div>

            <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;">
                <a href="#" class="site-menu-toggle js-menu-toggle float-right"><span class="icon-menu h3"></span>
                </a>
            </div>
        </div>
    </div>
</header>