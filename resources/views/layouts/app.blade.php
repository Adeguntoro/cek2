<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Fonts -->
        <link href="{{ asset('fonts/font.css') }}" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('css')
        <style>
            .navbar{
                min-height: 80px;
                transition: 0.5s;
            }
            .h2, h1, h2, h3, h4, h5 {
                font-family: 'Catamaran-ExtraBold' !important;
            }
        </style>
    </head>
    <body>
        <div id="app">
            @include('layouts.navbar')
            <main class="py-4">
                @yield('content')
            </main>
        </div>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        @yield('js')

    </body>
</html>
