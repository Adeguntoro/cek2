@extends('halaman_cek.app')

@section('title', 'Form Pengecekan Mobil')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
<section class="site-section" id="services-section">
    <div class="container">
        <div class="row justify-content-center batas_atas" data-aos="fade-up">
            <div class="col-lg-6 text-center heading-section mb-5">
                <h2 class="text-black mb-2">Cek Kendaraan<span class="text-danger">.</span></h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        
        <div class="row justify-content-center">

            <div class="col-8">
                <form method="post" action="/cek/mobil">
                    @csrf
                    
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Plat Nomor</label>
                        <input type="text" class="form-control @error('nmrplat') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Plat Nomor" aria-describedby="emailHelp" name="nmrplat" value="{{ old('nmrplat') }}">
                        @error('nmrplat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <small id="emailHelp" class="form-text text-muted">Tidak perlu menggunakan spasi</small>
                    </div>
                    
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nomor Rangka</label>
                        <input type="text" class="form-control @error('nmrrangka') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Nomor Rangka" name="nmrrangka" value="{{ old('nmrrangka') }}">
                        @error('nmrrangka')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <small id="emailHelp" class="form-text text-muted">Cukup masukan angka atau huruf saja</small>
                    </div>
                    
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nomor Mesin</label>
                        <input type="text" class="form-control @error('nmrmesin') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Nomor Mesin" name="nmrmesin" value="{{ old('nmrmesin') }}">
                        @error('nmrmesin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <small id="emailHelp" class="form-text text-muted">Cukup masukan angka atau huruf saja</small>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" >Kode Captcha</label>
                        <input id="captcha" type="captcha" class="form-control captcha @error('captcha') is-invalid @enderror" name="captcha" placeholder="Masukan kode Captcha">
                        @error('captcha')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <small id="emailHelp" class="form-text text-muted">Masukan kode Captcha dibawah</small>
                    
                        {{--
                        <br>
                            {!! captcha_img('flat') !!}
                        <button id="regen-captcha" class="btn">Reload Captcha</button>
                        --}}
                    </div>

                    <div class="form-group text-center">
                        {!! captcha_img('flat') !!}
                    </div>
                    <div class="text-center">
                        <button class="btn btn-primary" type="submit">Cek</button>
                    </div>
                    
                    
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>

    $('#regen-captcha').on('click', function(e){
        e.preventDefault();

        var anchor = $(this);
        var captcha = anchor.prev('img');

        $.ajax({
            type    : "GET",
            url     : "{{ url('captcha_server')}}",
        }).done(function( msg ) {
            captcha.attr('src', msg);
        });

    });
    
    $().button('toggle')

</script>
@endsection

