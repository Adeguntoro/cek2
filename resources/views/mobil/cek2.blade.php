@extends('halaman_cek.app')

@section('title', 'Form Pengecekan Mobil')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
<section class="site-section" id="services-section">
    <div class="container">
        <div class="row justify-content-center batas_atas" data-aos="fade-up">
            <div class="col-lg-6 text-center heading-section mb-5">
                <h2 class="text-black mb-2">Cek Mobil<span class="text-danger">.</span></h2>
                {{--
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> --}}
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-md-4 text-center mb-3" data-aos="fade-up" data-aos-delay="">
                <div class="block_service">
                    <img src="{{ url('/tema/images/dogger_checkup.svg') }}" alt="Image mb-5">
                    <h3>Cek berdasarkan plat</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <a href="/cek/kendaraan/mobil/form/plat" class="btn btn-primary stretched-link">Cek Sekarang...</a>
                </div>
            </div>
            <div class="col-md-4 text-center mb-3" data-aos="fade-up"  data-aos-delay="100">
                <div class="block_service">
                    <img src="{{ url('/tema/images/dogger_dermatology.svg') }}" alt="Image mb-5">
                    <h3>Cek berdasarkan nomor rangka</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <a href="/cek/kendaraan/mobil/form/rangka" class="btn btn-primary stretched-link">Cek Sekarang...</a>
                </div>
            </div>
            <div class="col-md-4 text-center mb-3" data-aos="fade-up"  data-aos-delay="100">
                <div class="block_service">
                    <img src="{{ url('/tema/images/dogger_dermatology.svg') }}" alt="Image mb-5">
                    <h3>Cek berdasarkan nomor mesin</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <a href="/cek/kendaraan/mobil/form/mesin" class="btn btn-primary stretched-link">Cek Sekarang...</a>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection