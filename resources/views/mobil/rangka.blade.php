@extends('halaman_cek.app')

@section('title', 'Form Pengecekan Rangka Mobil')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
<section class="site-section" id="services-section">
    <div class="container">
        <div class="row justify-content-center batas_atas" data-aos="fade-up">
            <div class="col-lg-6 text-center heading-section mb-5">
                <h2 class="text-black mb-2">Cek Mobil<span class="text-danger">.</span></h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4 text-center mb-3" data-aos="fade-up" data-aos-delay="">
                <div class="block_service">
                    <h3>Cek berdasarkan nomor rangka</h3>
                    
                    <form method="post" action="/cek/mobil">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="rangka" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="B168OOS">
                            <small id="emailHelp" class="form-text text-muted">Tidak perlu menggunakan spasi atau tanda.</small>
                        </div>
                        <button class="btn btn-primary">Cek Sekarang...</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection