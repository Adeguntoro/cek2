@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">Robots</div>

                <div class="card-body">
                    <form action="/robot/save" method="post">
                        @csrf
                        
                        <div class="form-group">
                        <textarea name="robot_txt"  class="form-control" rows="15" placeholder="Content of your robot.txt">{!! $read !!}</textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Save" class="btn btn-primary">
                        </div>
                    </form>
                                                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
