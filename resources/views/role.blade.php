{{--
@role('Master')
    I am a master
@elseifrole('Admin')
    I am a admin
@elseifrole('Staff')
    I am a staff
@endrole
--}}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @role('Master')
                        I am a master
                    @elserole('Admin')
                        I am a admin
                    @elserole('Staff')
                        I am a staff
                    @elserole('User')
                        A USER !!!!!!!!!!!
                    @else
                        I am a no one
                    @endrole
                    <br>

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
