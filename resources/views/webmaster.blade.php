@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">Dashboard Webmaster Tag</div>
                <div class="card-body">
                    <form method="post" action="{{ route('webmaster.store') }}">
                        @csrf
                        @foreach($datas as $data)
                        
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">{{ $data->name }}</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" name="settings[{{ $data->name_key }}][value]" value="{{ $data->value }}">
                            </div>
                        </div>
                        
                        @endforeach
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
