<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/



Route::get('/','IndexController@index');

Auth::routes([
    'reset' => false,
    'register'=> true,
    'verify' => false
]);

/*
Route::get('/role', function(){
    return view('role');
});

Route::get('/hole', function(){
    return view('home');
});
Route::get('/robot', 'Robots@index');
Route::post('/robot/save', 'Robots@save');

Route::get('/upload_web_image', 'SiteSettingController@webImage');
Route::post('crop_image', 'ProfileController@updateAvatar');

Route::resource('webmaster', 'WebmasterTagController');

Route::get('/index', 'IndexController@index');
Route::resource('site', 'SiteSettingController');
Route::get('/setting/edit/site/', 'SiteSettingController@formAdd');
Route::post('/setting/add/site/', 'SiteSettingController@formStore');
*/
//Auth
//Route::middleware(['auth', 'verified'])->group(function () {
Route::middleware(['auth'])->group(function () {
    //Route::middleware(['password.confirm'])->group(function () {
        //Profile
        Route::get('/{nomor_iden}/profile', 'ProfileController@show');
        Route::post('/{nomor_iden}/profile/update', 'ProfileController@update');
        //password
        Route::get('/{nomor_iden}/profile/password', 'ProfileController@passwordform');
        Route::post('/{nomor_iden}/profile/password/update', 'ProfileController@changePassword');
    //});
    
    Route::middleware(['CheckUserRole'])->group(function () {
        Route::get('sukses', function(){
            return 'sukses';
        });
    });
    Route::group(['middleware' => ['role:User']], function () {
        Route::get('/home', 'IndexController@home');
        //Halaman Cek
        Route::get('/cek/kendaraan', 'CekController@kendaraan');
        Route::get('/cek/kendaraan/motor', 'CekController@formMotor');
    });

    Route::prefix('dashboard')->group(function () {
        Route::group(['middleware' => ['role:Master']], function () {
            Route::get('/ocisly', function(){
                return 'master';
            });
        });
    
        Route::group(['middleware' => ['role:Admin']], function () {
            //Route::resource('/admin', '');
            Route::get('/admin', function(){
                return 'admin';
            });
        });
    
        Route::group(['middleware' => ['role:Staff']], function () {
            Route::get('/staff', function(){
                return 'staff';
            });
        });
    
        Route::group(['middleware' => ['role:Blogger']], function () {
            Route::get('/blogger', function(){
                return 'Blogger';
            });
        });

    });


});

//Route::get('/sitemap/gen', 'SiteSettingController@sitemap');