<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/


Route::get('/','IndexController@index');

Route::get('/faq','IndexController@faq');
Route::get('/donasi','IndexController@donasi');
Route::get('/home','IndexController@home');

Auth::routes([
    'reset' => false,
    'register'=> false,
    'verify' => false,
    'login' => false,

]);

/*

Route::middleware(['auth'])->group(function () {
        Route::get('/{nomor_iden}/profile', 'ProfileController@show');
        Route::post('/{nomor_iden}/profile/update', 'ProfileController@update');
        //password
        Route::get('/{nomor_iden}/profile/password', 'ProfileController@passwordform');
        Route::post('/{nomor_iden}/profile/password/update', 'ProfileController@changePassword');
    
    
    Route::middleware(['CheckUserRole'])->group(function () {
        Route::get('sukses', function(){
            return 'sukses';
        });
    });
    Route::group(['middleware' => ['role:User']], function () {
        Route::get('/home', 'IndexController@home');
        //Halaman Cek
        Route::get('/cek/kendaraan', 'CekController@kendaraan');
        Route::get('/cek/kendaraan/motor', 'CekController@formMotor');
    });

    Route::prefix('dashboard')->group(function () {
        Route::group(['middleware' => ['role:Master']], function () {
            Route::get('/ocisly', function(){
                return 'master';
            });
        });
    
        Route::group(['middleware' => ['role:Admin']], function () {
            //Route::resource('/admin', '');
            Route::get('/admin', function(){
                return 'admin';
            });
        });
    
        Route::group(['middleware' => ['role:Staff']], function () {
            Route::get('/staff', function(){
                return 'staff';
            });
        });
    
        Route::group(['middleware' => ['role:Blogger']], function () {
            Route::get('/blogger', function(){
                return 'Blogger';
            });
        });

    });


});

*/
