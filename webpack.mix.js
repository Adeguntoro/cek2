const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'public/tema/css/style.css',
        'public/tema/css/aos.css',
        'public/tema/css/bootstrap-datepicker.css',
        'public/tema/css/jquery.fancybox.min.css',
        'public/tema/css/owl.theme.default.min.css',
        'public/tema/css/owl.theme.default.min.css',
        'public/tema/css/owl.carousel.min.css',
        'public/tema/css/jquery-ui.css',
        'public/tema/css/bootstrap.min.css',
        'public/tema/fonts/icomoon/style.css',
    ], 'public/css/all.css')
    .scripts([
        'public/tema/js/jquery-3.3.1.min.js',
        'public/tema/js/jquery-ui.js',
        'public/tema/js/popper.min.js',
        'public/tema/js/bootstrap.min.js',
        'public/tema/js/owl.carousel.min.js',
        'public/tema/js/jquery.countdown.min.js',
        'public/tema/js/jquery.easing.1.3.js',
        'public/tema/js/aos.js',
        'public/tema/js/jquery.fancybox.min.js',
        'public/tema/js/jquery.sticky.js',
        'public/tema/js/isotope.pkgd.min.js',
        'public/tema/js/main.js',
    ], 'public/js/all.js');
